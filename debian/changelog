python-m2r (0.3.1-0.1) unstable; urgency=medium

  * Non-maintainer upload, with maintainer's permission.
  * New upstream release.
    - Compatible with docutils 0.19 (closes: #1026288).
  * Drop patches, no longer needed with the new release:
    - debian/patches/0001_fix_Python_3_10.patch
    - debian/patches/1001_modernize_sphinx.patch
  * Refresh 2001_privacy.patch and 2003_mistune0.patch.
  * Drop python3-mock build-dependency. This package uses unittest.mock.
  * Fix built-using-field-on-arch-all-package Lintian warning.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 10 Jan 2023 12:01:56 +0400

python-m2r (0.2.1-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add upstream patch for test failure with Python 3.10 (Closes: #1009396)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 20 Apr 2022 00:03:38 +0200

python-m2r (0.2.1-7) unstable; urgency=medium

  * add patch 2003 to use custom-named Python module mistune0

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Feb 2022 15:17:37 +0100

python-m2r (0.2.1-6) unstable; urgency=medium

  * (build-)depend on python3-mistune0 (not python3-mistune);
    closes: bug#1002163, #1003571,
    thanks to Lucas Nussbaum and Pierre-Elliott Bécue

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 24 Feb 2022 14:46:25 +0100

python-m2r (0.2.1-5) unstable; urgency=medium

  * update git-buildpage settings:
    + use DEP-14 git branch names
    + add usage comment
  * update patch 1001;
    closes: bug#975825,
    thanks to Lucas Nussbaum, Kyle Robbertze, and Daniel Caballero
  * use debhelper compatibility level 13 (not 12)
  * declare compliance with Debian Policy 4.5.1
  * update and unfuzz patches

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Dec 2020 17:36:21 +0100

python-m2r (0.2.1-4) unstable; urgency=medium

  * use debhelper compatibility level 12 (not 10);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.0
  * simplify source script copyright-chceck
  * copyright: update coverage
  * add patch cherry-picked upstream to modernize Sphinx support;
    closes: bug#963652, thanks to Lucas Nussbaum and Benjamin Hof

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Aug 2020 11:37:15 +0200

python-m2r (0.2.1-3) unstable; urgency=medium

  * re-release targeted unstable

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 28 Nov 2019 01:50:37 +0100

python-m2r (0.2.1-2) experimental; urgency=medium

  * set Testsuite: autopkgtest-pkg-python

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 13 Nov 2019 17:32:15 +0100

python-m2r (0.2.1-1) experimental; urgency=low

  * Initial packaging release.
    Closes: bug#943456.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Oct 2019 12:00:21 +0200
